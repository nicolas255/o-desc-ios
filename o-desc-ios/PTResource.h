//
//  PTResource.h
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PhysicalThing.h"

@interface PTResource : PhysicalThing

@property NSString *fileName;

@end
