//
//  PhysicalThing.h
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhysicalThing : NSObject

@property NSString *name;

- (id)initWithName:(NSString *)name;

@end

@interface PhysicalThing (ReadOnlyProperties)

//La propriété est en readOnly car si on implémente le stockage persistant, les élément seront générés par la méthode initWithID
@property (readonly, nonatomic) NSString *uniqueID;
@property (readonly, nonatomic) NSDate *dateCreation;

@end
