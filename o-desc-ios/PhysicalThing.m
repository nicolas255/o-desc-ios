//
//  PhysicalThing.m
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import "PhysicalThing.h"

@interface PhysicalThing ()

@property (readonly, nonatomic, retain) NSString *uniqueID;
@property (readonly, nonatomic, retain) NSDate *dateCreation;

@end

@implementation PhysicalThing

@synthesize uniqueID = _uniqueID;
@synthesize dateCreation = _dateCreation;

//Generate an unique ID
+ (NSString *)uuid
{
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge_transfer NSString *)uuidStringRef;
}

//Making sure that every things has an unique ID
- (id)init{
    self = [super init];
    if (self) {
        _uniqueID = [PhysicalThing uuid];
        _dateCreation = [NSDate date];
    }
    return self;
}

- (id)initWithName:(NSString *)name{
    self = [self init];
    if (self) {
        self.name = name;
    }
    return self;
}



@end
