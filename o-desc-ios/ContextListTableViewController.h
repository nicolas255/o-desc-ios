//
//  ContextListTableViewController.h
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTContext.h"

@interface ContextListTableViewController : UITableViewController

@property NSMutableArray *contexts;
@property PTContext *currentChild;

- (IBAction)unwindToListFromAdd:(UIStoryboardSegue *) segue;

@end
