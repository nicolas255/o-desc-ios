//
//  ObjectListTableViewController.h
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTObject.h"

@interface ObjectListTableViewController : UITableViewController

@property NSMutableArray *objects;
@property PTObject *currentChild;

- (IBAction)unwindToListFromAdd:(UIStoryboardSegue *) segue;

@end
