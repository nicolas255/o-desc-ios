//
//  PTObject.m
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import "PTObject.h"

@implementation PTObject

-(id)init{
    self = [super init];
    
    if (self) {
        self.children = [[NSMutableArray alloc] init];
    }
    
    return self;
}

@end
