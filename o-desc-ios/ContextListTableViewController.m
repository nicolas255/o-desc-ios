//
//  ContextListTableViewController.m
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import "ContextListTableViewController.h"
#import "PTContext.h"
#import "PTObject.h"
#import "PTResource.h"
#import "AddContextViewController.h"
#import "ObjectListTableViewController.h"

@interface ContextListTableViewController ()

@end

@implementation ContextListTableViewController

//Manage the "comming back here" event
- (IBAction)unwindToListFromAdd:(UIStoryboardSegue *)segue{
    AddContextViewController *source = [segue sourceViewController];
    PTContext *newItem = source.addedContext;
    if (newItem != nil) {
        [self.contexts addObject:newItem];
    }
    [self.tableView reloadData];
}

//Well... It's in the name...
-(void)loadInitialSampleData{
    PTContext *context1 = [[PTContext alloc] initWithName:@"Chambre"];
    [self.contexts addObject:context1];
    
    
    PTObject *object11 = [[PTObject alloc] initWithName:@"Lit"];
    [context1.children addObject:object11];
    
    PTResource *resource111 = [[PTResource alloc] initWithName:@"De devant"];
    [object11.children addObject:resource111];
    
    PTResource *resource112 = [[PTResource alloc] initWithName:@"Coté gauche"];
    [object11.children addObject:resource112];
    
    PTResource *resource113 = [[PTResource alloc] initWithName:@"Coté droit"];
    [object11.children addObject:resource113];
    
    PTResource *resource114 = [[PTResource alloc] initWithName:@"De derrière"];
    [object11.children addObject:resource114];
    
    
    PTObject *object12 = [[PTObject alloc] initWithName:@"Bureau"];
    [context1.children addObject:object12];
    
    PTResource *resource121 = [[PTResource alloc] initWithName:@"Vide"];
    [object12.children addObject:resource121];
    
    PTResource *resource122 = [[PTResource alloc] initWithName:@"Avec les écrans"];
    [object12.children addObject:resource122];
    
    
    
    
    PTContext *context2 = [[PTContext alloc] initWithName:@"Salle d'entrée"];
    [self.contexts addObject:context2];
    
    PTObject *object21 = [[PTObject alloc] initWithName:@"Porte"];
    [context2.children addObject:object21];
    
    PTResource *resource211 = [[PTResource alloc] initWithName:@"Fermée"];
    [object21.children addObject:resource211];
    
    PTResource *resource212 = [[PTResource alloc] initWithName:@"Ouverte"];
    [object21.children addObject:resource212];
    
    
    PTObject *object22 = [[PTObject alloc] initWithName:@"Porte manteau"];
    [context2.children addObject:object22];
    
    
    
    
    PTContext *context3 = [[PTContext alloc] initWithName:@"Garage"];
    [self.contexts addObject:context3];
    
    PTObject *object31 = [[PTObject alloc] initWithName:@"Voiture"];
    [context3.children addObject:object31];
    
    PTObject *object32 = [[PTObject alloc] initWithName:@"Prise électrique de Zoe"];
    [context3.children addObject:object32];
    
    
    
    
    PTContext *context4 = [[PTContext alloc] initWithName:@"Cuisine"];
    [self.contexts addObject:context4];
    
    PTObject *object41 = [[PTObject alloc] initWithName:@"Réfrigérateur"];
    [context4.children addObject:object41];
    
    PTObject *object42 = [[PTObject alloc] initWithName:@"Plaque de cuisson"];
    [context4.children addObject:object42];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contexts = [[NSMutableArray alloc] init];
    [self loadInitialSampleData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contexts count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContextPrototypeCell" forIndexPath:indexPath];
    
    PTContext *ptContext = [self.contexts objectAtIndex:indexPath.row];
    cell.textLabel.text = ptContext.name;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.contexts removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.currentChild = [self.contexts objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = self.storyboard;
    ObjectListTableViewController *destination = [storyboard instantiateViewControllerWithIdentifier:@"ObjectList"];
    destination.objects = self.currentChild.children;

    [self.navigationController pushViewController:destination animated:YES];
}

@end
