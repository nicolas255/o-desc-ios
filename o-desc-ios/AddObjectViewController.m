//
//  AddObjectViewController.m
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import "AddObjectViewController.h"

@interface AddObjectViewController ()

@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation AddObjectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if (sender != self.saveButton) return;
    
    if (self.textField.text.length > 0) {
        self.addedObject = [[PTObject alloc] initWithName:self.textField.text];
    }

}

@end
