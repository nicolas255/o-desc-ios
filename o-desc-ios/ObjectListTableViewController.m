//
//  ObjectListTableViewController.m
//  o-desc-ios
//
//  Created by Nicolas Klein on 10/09/15.
//  Copyright (c) 2015 RCDSM. All rights reserved.
//

#import "ObjectListTableViewController.h"
#import "PTObject.h"
#import "PTContext.h"
#import "AddObjectViewController.h"
#import "ContextListTableViewController.h"
#import "ResourceListTableViewController.h"

@interface ObjectListTableViewController ()

@end

@implementation ObjectListTableViewController

//Manage the "comming back here" event
- (IBAction)unwindToListFromAdd:(UIStoryboardSegue *)segue{
    AddObjectViewController *source = [segue sourceViewController];
    PTObject *newItem = source.addedObject;
    if (newItem != nil) {
        [self.objects addObject:newItem];
    }
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.objects count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ObjectPrototypeCell" forIndexPath:indexPath];

    PTObject *ptObject = [self.objects objectAtIndex:indexPath.row];
    cell.textLabel.text = ptObject.name;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    self.currentChild = [self.objects objectAtIndex:indexPath.row];
    
    UIStoryboard *storyboard = self.storyboard;
    ResourceListTableViewController *destination = [storyboard instantiateViewControllerWithIdentifier:@"ResourceList"];
    destination.resources = self.currentChild.children;
    
    [self.navigationController pushViewController:destination animated:YES];
}

@end
